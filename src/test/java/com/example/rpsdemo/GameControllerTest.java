package com.example.rpsdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class GameControllerTest {

    @Autowired
    GameController gameController;

    @MockBean
    GameService gameService;

    @Test
    void createGame() {
        // Given
        Game game = new Game();
        when(gameService.createGame()).thenReturn(game);

        // When
        String gameId = gameController.createGame();

        // Then
        assertEquals(game.getId(), gameId);
    }

}
