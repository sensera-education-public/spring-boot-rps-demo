package com.example.rpsdemo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class GameIntegrationTest {
    private static final String BASE_URL = "/api/rps/";

    @Autowired
    WebTestClient webTestClient;

    @Test
    void createGame() {
        String gameId = createGameRequest();

        assertNotNull(gameId);
    }

    @Test
    void joinGame() {
        String gameId = createGameRequest();

        String joinGameHandle = joinGameRequest(gameId);

        assertNotNull(joinGameHandle);
    }

    @Test
    void makeMoveOwner() {
        String gameId = createGameRequest();
        String joinGameHandle = joinGameRequest(gameId);

        GameStatus status = makeMoveRequest(gameId, "ROCK");

        assertEquals(gameId, status.getGameId());
        assertEquals(joinGameHandle, status.getJoinGameHandle());
        assertEquals("ROCK", status.getOwnerMove());
        assertEquals("", status.getJoinerMove());
        assertEquals("", status.getResult());
    }

    @Test
    void makeMoveJoiner() {
        String gameId = createGameRequest();
        String joinGameHandle = joinGameRequest(gameId);

        GameStatus status = makeMoveRequest(joinGameHandle, "ROCK");

        assertEquals(gameId, status.getGameId());
        assertEquals(joinGameHandle, status.getJoinGameHandle());
        assertEquals("", status.getOwnerMove());
        assertEquals("ROCK", status.getJoinerMove());
        assertEquals("", status.getResult());
    }

    @Test
    void playGame() {
        String gameId = createGameRequest();
        String joinGameHandle = joinGameRequest(gameId);
        makeMoveRequest(gameId, "PAPER");
        makeMoveRequest(joinGameHandle, "ROCK");

        GameStatus status = getGameStatusRequest(gameId);

        assertEquals(gameId, status.getGameId());
        assertEquals(joinGameHandle, status.getJoinGameHandle());
        assertEquals("PAPER", status.getOwnerMove());
        assertEquals("ROCK", status.getJoinerMove());
        assertEquals("WIN", status.getResult());
    }

    private GameStatus makeMoveRequest(String gameId, String move) {
        return webTestClient.get().uri(BASE_URL + gameId + "/move/" + move)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(GameStatus.class)
                .getResponseBody()
                .single()
                .block();
    }
    private GameStatus getGameStatusRequest(String gameId) {
        return webTestClient.get().uri(BASE_URL + gameId + "/status/")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(GameStatus.class)
                .getResponseBody()
                .single()
                .block();
    }

    private String createGameRequest() {
        return webTestClient.get().uri(BASE_URL)
                .accept(MediaType.TEXT_PLAIN)
                .exchange()
                .expectStatus().isOk()
                .returnResult(String.class)
                .getResponseBody()
                .single()
                .block();
    }

    private String joinGameRequest(String gameId) {
        return webTestClient.get().uri(BASE_URL + gameId + "/join/")
                .accept(MediaType.TEXT_PLAIN)
                .exchange()
                .expectStatus().isOk()
                .returnResult(String.class)
                .getResponseBody()
                .single()
                .block();
    }

    @Value
    private static class GameStatus {
        String gameId;
        String joinGameHandle;
        String ownerMove;
        String joinerMove;
        String result;

        @JsonCreator
        public GameStatus(@JsonProperty("gameId") String gameId,
                          @JsonProperty("joinGameHandle") String joinGameHandle,
                          @JsonProperty("ownerMove") String ownerMove,
                          @JsonProperty("joinerMove") String joinerMove,
                          @JsonProperty("result") String result) {
            this.gameId = gameId;
            this.joinGameHandle = joinGameHandle;
            this.ownerMove = ownerMove;
            this.joinerMove = joinerMove;
            this.result = result;
        }
    }
}
