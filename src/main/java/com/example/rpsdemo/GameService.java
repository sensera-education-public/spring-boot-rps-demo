package com.example.rpsdemo;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

@Service
public class GameService {
    Map<String,Game> games = new HashMap<>();

    public Game createGame() {
        Game game = new Game();
        games.put(game.getId(), game);
        return game;
    }

    public Game joinGame(String gameId) throws GameAlreadyJoinedException {
        Game game = games.get(gameId);
        game.join();
        return game;
    }

    public Game makeMove(String gameIdOrJoinerHandle, Move move) throws GameNotFoundException {
        Game game = games.get(gameIdOrJoinerHandle);
        if (game == null) {
            game = games.values().stream()
                    .filter(g -> Objects.equals(g.getJoinerHandle(),gameIdOrJoinerHandle))
                    .findAny()
                    .orElse(null);
            if (game != null)
                game.setJoinerMove(move);
            else
                throw new GameNotFoundException();
        } else
            game.setOwnerMove(move);
        return checkWinner(game);
    }

    private Game checkWinner(Game game) {
        if (game.getOwnerMove() == null || game.getJoinerMove() == null)
            return game;
        game.setGameResult(calculateGameResult(game.getOwnerMove(), game.getJoinerMove()));
        return game;
    }

    private GameResult calculateGameResult(Move ownerMove, Move joinerMove) {
        if (ownerMove == joinerMove)
            return GameResult.DRAW;
        switch (ownerMove) {
            case SCISSORS: return joinerMove.equals(Move.PAPER) ? GameResult.WIN : GameResult.LOSE;
            case PAPER: return joinerMove.equals(Move.ROCK) ? GameResult.WIN : GameResult.LOSE;
            case ROCK: return joinerMove.equals(Move.SCISSORS) ? GameResult.WIN : GameResult.LOSE;
        }
        throw new RuntimeException("Internal error!");
    }

    public Game getStatus(String gameIdOrJoinerHandle) throws GameNotFoundException {
        Game game = games.get(gameIdOrJoinerHandle);
        if (game == null) {
            game = games.values().stream()
                    .filter(g -> Objects.equals(g.getJoinerHandle(), gameIdOrJoinerHandle))
                    .findAny()
                    .orElse(null);
        }
        if (game == null)
            throw new GameNotFoundException();
        return game;
    }
}
