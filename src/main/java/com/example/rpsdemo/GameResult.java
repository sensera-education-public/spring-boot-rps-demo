package com.example.rpsdemo;

public enum GameResult {
    WIN,
    LOSE,
    DRAW
}
