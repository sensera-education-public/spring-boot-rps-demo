package com.example.rpsdemo;

import lombok.Value;

@Value
public class GameStatusDTO {
    String gameId;
    String joinGameHandle;
    String ownerMove;
    String joinerMove;
    String result;
}
