package com.example.rpsdemo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/rps")
@AllArgsConstructor
public class GameController {

    GameService gameService;

    @GetMapping()
    public String createGame() {
        return gameService.createGame()
                .getId();
    }

    @GetMapping("/{gameId}/join")
    public ResponseEntity<String> joinGame(@PathVariable("gameId") String gameId) {
        try {
            return ResponseEntity.ok(gameService.joinGame(gameId)
                    .getJoinerHandle());
        } catch (GameAlreadyJoinedException e) {
            return ResponseEntity.badRequest()
                    .build();
        }
    }

    @GetMapping("/{gameId}/move/{move}")
    public ResponseEntity<GameStatusDTO> makeMove(@PathVariable("gameId") String gameId, @PathVariable("move") String move) {
        try {
            return ResponseEntity.ok(toGameStatus(gameService.makeMove(gameId, Move.valueOf(move))));
        } catch (GameNotFoundException e) {
            return ResponseEntity.notFound()
                    .build();
        }
    }

    @GetMapping("/{gameId}/status")
    public ResponseEntity<GameStatusDTO> status(@PathVariable("gameId") String gameId) {
        try {
            return ResponseEntity.ok(toGameStatus(gameService.getStatus(gameId)));
        } catch (GameNotFoundException e) {
            return ResponseEntity.notFound()
                    .build();
        }
    }

    private GameStatusDTO toGameStatus(Game game) {
        return new GameStatusDTO(
                game.getId(),
                game.getJoinerHandle() != null ? game.getJoinerHandle() : "",
                game.ownerMove()
                        .map(Enum::name)
                        .orElse(""),
                game.joinerMove()
                        .map(Enum::name)
                        .orElse(""),
                game.gameResult()
                        .map(Enum::name)
                        .orElse("")
        );
    }
}
