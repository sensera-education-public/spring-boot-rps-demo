package com.example.rpsdemo;

import lombok.Data;

import java.util.Optional;
import java.util.UUID;

@Data
public class Game {
    String id;
    String joinerHandle;
    Move ownerMove;
    Move joinerMove;
    GameResult gameResult;

    public Game() {
        this.id = UUID.randomUUID().toString();
    }

    public void join() throws GameAlreadyJoinedException {
        if (joinerHandle != null)
            throw new GameAlreadyJoinedException();
        joinerHandle = UUID.randomUUID().toString();
    }

    public Optional<Move> ownerMove() { return Optional.ofNullable(ownerMove); }
    public Optional<Move> joinerMove() { return Optional.ofNullable(joinerMove); }

    public Optional<GameResult> gameResult() {
        return Optional.ofNullable(gameResult);
    }
}
